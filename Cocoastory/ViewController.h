//
//  ViewController.h
//  Cocoabook
//
//  Created by cetauri on 13. 5. 6..
//  Copyright (c) 2013년 KT. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "REComposeViewController.h"

@interface ViewController : UIViewController<REComposeViewControllerDelegate>{
    
    IBOutlet UIButton *button;
}

@end
